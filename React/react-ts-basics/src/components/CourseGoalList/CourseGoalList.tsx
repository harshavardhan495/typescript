import CourseGoal from "../CourseGoal/CourseGoal.tsx";
import { type courseGoal as cGoal} from '../../App.tsx';
import "./CourseGoalList.css";

type CourseGoalListProps = {
  goals: cGoal[];
  onDeleteGoal: (id:number)=>void;
};

export default function CourseGoalList({goals,onDeleteGoal}:CourseGoalListProps) {
  return (
    <ul className="card-container">
      {goals.map((goal) => (
        <li key={goal.id}>
          <CourseGoal title={goal.title} id={goal.id} onDelete={onDeleteGoal}>
            <p>{goal.description}</p>
          </CourseGoal>
        </li>
      ))}
    </ul>
  );
}
