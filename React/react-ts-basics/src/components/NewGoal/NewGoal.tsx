import { useRef, FormEvent} from "react";
import "./NewGoal.css";

type NewGoalProps = {
  onAddGoal: (goal: string, summary: string) => void;
};

export default function NewGoal({ onAddGoal }: NewGoalProps) {
  const goal = useRef<HTMLInputElement>(null);
  const summary = useRef<HTMLInputElement>(null);

  function handleSubmit(event: FormEvent<HTMLFormElement>) {
    event?.preventDefault();

    const enteredGoal = goal.current!.value;
    const enteredSummary = goal.current!.value;

    event.currentTarget.reset();

    onAddGoal(enteredGoal, enteredSummary);
  }
  return (
    <form onSubmit={handleSubmit}>
      <ul>
        <li>
          <label htmlFor="goal">Goal:</label> <br />
          <input id="goal" type="text" ref={goal} />
        </li>
        <li>
          <label htmlFor="summary">Summary:</label> <br />
          <input id="summary" type="text" ref={summary} />
        </li>
        <li>
          <button>Add Goal</button>
        </li>
      </ul>
    </form>
  );
}
