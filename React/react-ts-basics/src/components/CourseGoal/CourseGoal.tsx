import { type FC, type PropsWithChildren } from "react";
import "./CourseGoal.css";

// interface CourseGoalProps{
//     title:string;
//     children: ReactNode;
// }

type CourseGoalProps = PropsWithChildren<{ title: string, id:number,  onDelete: (id:number)=>void; }>;

// export default function CourseGoal({title, children}:CourseGoalProps){
//     return <article>
//         <div className="card">
//             <h1>{title}</h1>
//             {children}
//             <button>Delete</button>
//         </div>
//     </article>
// }


//CourseGoal is of type FC : Functional component 
const CourseGoal : FC <CourseGoalProps> = ({title,children, id , onDelete}) => {
  return (
    <article>
      <div className="card">
        <h1>{title}</h1>
        {children}
        <button onClick={()=>onDelete(id)}>Delete</button>
      </div>
    </article>
  );
};

export default CourseGoal;
