import Header from './components/Header/Header.tsx';
import CourseGoalList from './components/CourseGoalList/CourseGoalList.tsx';
import NewGoal from './components/NewGoal/NewGoal.tsx';
import reactImg from './assets/reactImg.png';
import './App.css'
import { useState } from 'react'; 

export type courseGoal = {
  title:string;
  description: string;
  id: number;
}

function App() {

  const [goals,setGoals] = useState<courseGoal[]>([]);

  function addCourseGoal(goal:string,summary:string){
    setGoals((prevGoal)=>{
      const newGoal: courseGoal = {
        title:goal,
        description:summary,
        id:Math.random(),
      }
      return [...prevGoal,newGoal]
    });
  }

  function handleDeleteGoal(id:number){
    setGoals((prevGoals)=> prevGoals.filter((goal)=>goal.id!==id));
  }

  return (
    <div className="body-container">
      <div className="main-container">
        <Header image={{ src: reactImg, alt: "React Image" }}>
          <h1>Lets get started</h1>
        </Header>
        <NewGoal onAddGoal={addCourseGoal} />
        <CourseGoalList goals={goals} onDeleteGoal={handleDeleteGoal} />
      </div>
    </div>
  );
}

export default App
