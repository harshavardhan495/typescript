export async function get(url: string){

    const response = await fetch(url,{
        method: 'GET'
    })

    if(!response.ok){
        throw new Error("Error during fetch");
        
    }

    const data = await response.json() as unknown;

    return data;

}