import { ReactNode, useEffect, useState } from "react";
import BlogPosts, {BlogPost} from './components/BlogPosts.tsx';
import { get } from "./utils/http.ts";
import fetchImg from './assets/data-fetching.png';
import ErrorMessage from "./components/ErrorMessage.tsx";

function App() {

  type RawBlogPostData = {
    id: number;
    title: string;
    body: string
    userId: number;
  }

  let content: ReactNode;

 const [fetchedBlogPosts, setBlogPosts] =  useState<BlogPost[]>();
 const [error,setError] = useState<string>();
 const[isFetching, setIsFetching] = useState<boolean>();
 useEffect(()=>{

  async function fetchPosts(){
    setIsFetching(true);
    try{
      const data = await get('https://jsonplaceholder.typicode.com/posts') as RawBlogPostData[];
  
      const blogPosts = data.map((rawPost)=>{
        return {
          id: rawPost.id,
          title: rawPost.title,
          text: rawPost.body,
        }
      });
  
      setBlogPosts(blogPosts);
    }catch(error){
      setError((error as Error).message);
    }
  }
  
  setIsFetching(false);
  fetchPosts();
 },[]);

 if(error){
  content = <ErrorMessage text={error}/>
 }

 if(isFetching){
  content = <p id="loading-fallback">Fetching posts....</p>
 }

 if(fetchedBlogPosts){
    content = <BlogPosts posts={fetchedBlogPosts}/>
 }

  return <main>
    <img src={fetchImg} alt="fetch-img"/>
    {
      content
    }
  </main>
}

export default App;
