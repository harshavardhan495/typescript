import { type ReactNode, createContext, useContext, useReducer } from "react";

export type Timers = {
    name: string;
    duration : number;
}

type TimersState = {
    isRunning: boolean;
    timers : Timers[];
}

type TimerContextValue = TimersState & {
    
    addTimer : (timerData: Timers)=>void,
    startTimers :()=>void,
    stopTimers:()=>void,
}

type StartTimerAction = {
    type: 'START_TIMERS';
}

type StopTimerAction = {
    type: 'STOP_TIMERS';
}

type AddTimerAction = {
    type: 'ADD_TIMER';
    payload: Timers;
}



type Action = StartTimerAction | StopTimerAction | AddTimerAction;

const initialState : TimersState = {
    isRunning: true,
    timers:[]
}

const TimersContext = createContext<TimerContextValue | null>(null);

type TimerContextProviderProps = {
    children: ReactNode 
}

export function useTimersContext(){    //created a custom hook for useContext with type guarding

    const timersCtx = useContext(TimersContext);

    if(timersCtx === null){
        throw Error("Timers Context value is null");
    }

    return timersCtx;
}

function timerReducer(state : TimersState,action:Action): TimersState{
    if(action.type === 'START_TIMERS'){
        return {
            ...state,
            isRunning:true
        }
    }
    if(action.type === 'STOP_TIMERS'){
        return {
            ...state,
            isRunning:false
        }
    }
    if(action.type === 'ADD_TIMER'){
        return {
            ...state,
            timers:[
                ...state.timers,
                {
                    name:action.payload.name,
                    duration: action.payload.duration
                }
            ]
        }
    }
    return state;
}

export default function TimersContextProvider({children}:TimerContextProviderProps){

    const [timersState,dispatch] = useReducer(timerReducer,initialState);
    const ctx : TimerContextValue = {
        timers: timersState.timers,
        isRunning: timersState.isRunning,
        addTimer(timerData) {
            dispatch({type:'ADD_TIMER',payload:timerData});
        },
        startTimers() {
            dispatch({type:'START_TIMERS'});
        },
        stopTimers() {
            dispatch({type:'STOP_TIMERS'});
        },

    }
    return <TimersContext.Provider value={ctx}>
            {children}
        </TimersContext.Provider>
}

