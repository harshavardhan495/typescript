import {
  ComponentPropsWithoutRef,
  FormEvent,
  useImperativeHandle,
  useRef,
  forwardRef
} from "react";

type FormProps = ComponentPropsWithoutRef<"form"> & {
  onSave: (data: unknown) => void;
};

export type FormType = {
    clear: ()=>void;
}

const Form = forwardRef<FormType,FormProps>(function Form({
  onSave,
  children,
  ...otherProps
} , ref) {
  const form = useRef<HTMLFormElement>(null);

  useImperativeHandle(ref, () => {
    return {
      clear() {
        console.log("CLEARING");    
        form.current?.reset();
      },
    };
  });

  function onSubmit(event: FormEvent<HTMLFormElement>) {
    event.preventDefault();
    const formData = new FormData(event.currentTarget);
    const data = Object.fromEntries(formData);
    onSave(data);
  }
  return (
    <form onSubmit={onSubmit} {...otherProps} ref={form}>
      {children}
    </form>
  );
});

export default Form;
