import Input from "./components/Input.tsx";
import Button from "./components/Button.tsx";
import Form from "./components/Form.tsx";
import { type FormType } from "./components/Form.tsx";
import {useRef} from 'react';

function App() {

  const customForm = useRef<FormType>(null);

  function onSave(data : unknown){
    // const submittedData = data as {name: string, age: number};
    //type guarding can be used when we are not sure about the type of value untill typescript is able to infer the final type
    if (
      !data ||
      typeof data !== 'object' ||
      !('name' in data) ||
      !('age' in data)
    ) {
        return;
    }
    console.log(data) 
    customForm.current?.clear();
  }

  return (
    <main>
      <Form onSave={onSave} ref={customForm}>
        <Input label="name" id="name" type="text"/>
        <Input label="age"  id="age" type="number"/>
        <p>
          <Button className="button">Save</Button>
        </p>
      </Form>
    </main>
  );
}

export default App;
