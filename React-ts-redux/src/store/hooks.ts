import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import {AppDispatchType,RootState} from './store';

type typeDispatchFunction = ()=>AppDispatchType

export const useDispatchFunction : typeDispatchFunction = useDispatch;

export const useCartSelector: TypedUseSelectorHook<RootState> = useSelector;
