var __assign = (this && this.__assign) || function () {
    __assign = Object.assign || function(t) {
        for (var s, i = 1, n = arguments.length; i < n; i++) {
            s = arguments[i];
            for (var p in s) if (Object.prototype.hasOwnProperty.call(s, p))
                t[p] = s[p];
        }
        return t;
    };
    return __assign.apply(this, arguments);
};
var _this = this;
var userName = "harsha";
var userAge = 25;
var flag = true;
var API_KEY = '6Harsha';
var userId = 'harsga6';
var user;
user = {
    name: "Harsha",
    age: 25,
    profession: "SDE-2"
};
//let hobbies:Array<string>;
var hobbies; //number[],boolean[], {name:string,age:number}[]
hobbies = ['harsha', 'vardhan'];
function add(a, b) {
    var result = a + b;
    console.log(result);
    return result;
}
function calculate(a, b, add) {
    add(a, b);
}
calculate(2, 3, add);
var creds;
creds = {
    userName: "admin",
    password: "Harsha123",
    mode: "Phone"
};
//interfaces vs custom types
//interfaces cannot be used to define function types
//interfaces are easily extendentable 
var AuthCredentials = /** @class */ (function () {
    function AuthCredentials() {
    }
    return AuthCredentials;
}());
function login(credentials) {
    console.log(credentials);
}
login(creds);
var admin;
admin = {
    userName: "harsha",
    permissions: ['LOGIN']
};
var infra;
infra = {
    permissions: ['LOGS', 'LOGIN', 'SUPERUSER'],
    userName: 'Harsha'
};
//literal types
var role;
role = 'ADMIN';
role = 'USER';
role = 'EDITOR';
function takeAction(action, role) {
    if (role === 'ADMIN') {
        //..
    }
}
var roles;
roles = ['ADMIN', 'EDITOR', 'USER']; // roles will only accept these values since the type is "Role"
//above DataStorage can expect multiple types at once <T,U..> where T and U represent types
var textStorage = {
    storage: [],
    add: function (data) {
        _this.storage.push(data);
    }
};
var idStorage = {
    storage: [],
    add: function (data) {
        console.log(data);
    }
};
var userStorage = {
    storage: [{ name: "harsha", age: 25, profession: 'SDE-2' }],
    add: function (data) {
        console.log(data);
    }
};
//generic function which can work for multiple types
function merge(a, b) {
    return __assign(__assign({}, a), b);
}
var newUser = merge({ name: "Harsha"
}, { age: 35 });
