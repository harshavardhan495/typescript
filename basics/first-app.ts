let userName = "harsha";
let userAge = 25;
let flag = true;
const API_KEY = '6Harsha';

//userId is either of type string or number
//usage of custom type

type stringOrNum = string | number;

let userId: stringOrNum = 'harsga6';

//custom type
type User = {
    name : string;
    age : number ;
    profession : string;
}

let user:User;

user = {
    name : "Harsha",
    age : 25,
    profession : "SDE-2"
}

//let hobbies:Array<string>;
let hobbies:string[] //number[],boolean[], {name:string,age:number}[]

hobbies = ['harsha','vardhan'];


function add(a:number,b:number):number{  //return void then use :void 
    const result = a+b;
    console.log(result);
    return result;
}

type AddFun = (a:number,b:number)=>number;

function calculate(a:number,b:number,add : AddFun){
   add(a,b);
}

calculate(2,3,add);

//defining object type with interfaces - it is to define objects

interface Credentials {
 userName:string;
 password: stringOrNum;
}

//extending interface
interface Credentials{
    mode:string;
}

let creds: Credentials;

creds = {
    userName:"admin",
    password:"Harsha123",
    mode:"Phone"
}

//interfaces vs custom types
//interfaces cannot be used to define function types
//interfaces are easily extendentable 

class AuthCredentials implements Credentials{
    userName: string;
    password: stringOrNum;
    mode:string;
    email:string;
}

function login(credentials: Credentials){
    console.log(credentials);
}

login(creds);
//or you can implement login like below
//login(new AuthCredentials());


//merging types

type Admin = {
    userName: string;
}

type AppUser = {
    permissions: string[];
}


type AppAdmin = Admin & AppUser // merge type

let admin : AppAdmin;

admin = {
    userName: "harsha",
    permissions:['LOGIN']
}

//merging interfaces

interface InfraAdmin {
    permissions : string[];
}

interface InfraUser {
    userName: string;
}

interface AppInfra extends InfraAdmin, InfraUser{

}

let infra : AppInfra;

infra = {
    permissions : ['LOGS','LOGIN','SUPERUSER'],
    userName : 'Harsha'
}

//custom type

type Role = "ADMIN" | "USER" | "EDITOR";

//literal types
let role: Role;

role = 'ADMIN';
role = 'USER';
role = 'EDITOR';

function takeAction(action:string,role:Role): void{
    if(role === 'ADMIN'){
      //..
    }
}

let roles : Array<Role>;

roles = ['ADMIN','EDITOR','USER']; // roles will only accept these values since the type is "Role"


//types where you can expect different types bundled together and having a generic type template to decide the type later depending on usage

type DataStorage<T>= {
    storage : T[];
    add: (data: T)=>void;
}

//above DataStorage can expect multiple types at once <T,U..> where T and U represent types

const textStorage : DataStorage<string> = {
    storage : [],
    add : (data)=>{
        this.storage.push(data);
    }
}

const idStorage : DataStorage<number>={
    storage: [],
    add:(data)=>{
        console.log(data);
    }
}

const userStorage : DataStorage<User>={ //using custom User type
    storage: [{ name:"harsha",age:25,profession:'SDE-2'}],
    add:(data)=>{
        console.log(data);
    }
}

//generic function which can work for multiple types

function merge<T,U>(a:T,b:U){
    return {
        ...a,...b
    }
}

const newUser = merge({name:"Harsha"
},{age:35});

