import { type ReactNode } from "react";
import './InfoBox.css';

type HintBoxProps = {
    mode:"hint";
    children:ReactNode;
}

type WarningBoxProps = {
    mode:'warning';
    severity: "high" | "medium" | "low";
    children: ReactNode;
}

type InfoBoxProps = HintBoxProps | WarningBoxProps;

export default function InfoBox(props:InfoBoxProps){
    const {mode,children} = props;
    if(mode === 'hint'){
        return <aside className="infobox infobox-hint">
            {children}
        </aside>
    }
  
    const {severity} = props;
    return (
        <aside className={`infobox-warning warning--${severity}`}>
            <h2>Warning</h2>
            {children}
        </aside>
    )
}

