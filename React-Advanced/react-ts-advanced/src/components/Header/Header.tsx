import {type FC, type PropsWithChildren, type ReactNode  } from "react";
import './Header.css';

// interface HeaderProps{
//   image: {
//      src: string;
//      alt:string;
//    };
//    children : ReactNode;
// }

type HeaderProps = PropsWithChildren<{image:{ src: string,alt:string}}>;

// export default function Header({image , children}:HeaderProps){
//   return (
//     <header>
//       <img src={image.src} alt = {image.alt} className="image"/>
//       {children}
//     </header>
//   )
// }

const Header : FC<HeaderProps> = ({image,children})=>{
  return (
    <header>
      <img src={image.src} alt={image.alt} className="image"/>
      {children}
    </header>
  )
}

export default Header;