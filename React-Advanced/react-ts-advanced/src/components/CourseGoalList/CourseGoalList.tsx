import CourseGoal from "../CourseGoal/CourseGoal.tsx";
import { type courseGoal as cGoal} from '../../App.tsx';
import InfoBox from "../InfoBox/InfoBox.tsx";
import "./CourseGoalList.css";
import { ReactNode } from "react";

type CourseGoalListProps = {
  goals: cGoal[];
  onDeleteGoal: (id:number)=>void;
};

export default function CourseGoalList({goals,onDeleteGoal}:CourseGoalListProps) {
  
  let warningBox : ReactNode;

  if(goals.length === 0){
    return <InfoBox mode='hint'>
      <p>You have no goal setup. Get started with few goals</p>
    </InfoBox>
  }else if(goals.length>3){
      warningBox = <InfoBox mode='warning' severity='high'>
        <p>You have added more goals. Reduce some to concentrate more on the existing ones</p>
      </InfoBox>
  }

  return (
    <ul className="card-container">
      <>
      {warningBox}
      {goals.map((goal) => (
        <li key={goal.id}>
          <CourseGoal title={goal.title} id={goal.id} onDelete={onDeleteGoal}>
            <p>{goal.description}</p>
          </CourseGoal>
        </li>
      ))}
      </>
    </ul>
  );
}
